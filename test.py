import whois
from auth import get_service_client



# The ID and range of a sample spreadsheet on Foubs
SAMPLE_SPREADSHEET_ID = "1oqKlwoBojQG1n2seNftOKNYKSPLS0sCAsrMMSjmQiVE"
SAMPLE_RANGE_NAME = "with_google!Y2:Y"
service=get_service_client('ab@sullivan-c.com','sheets','v4')

# Call the Sheets API
sheet = service.spreadsheets()
result = (
    sheet.values()
        .get(spreadsheetId=SAMPLE_SPREADSHEET_ID, range=SAMPLE_RANGE_NAME)
        .execute()
)
# print(sheet.)
values = result.get("values", [])
print(values)
if not values:
    print("No data found.")
else:
    i=0
    result = []
    for host in values:
        # i+=1
        # if i==50:
        #     break
        try:
            res=whois.whois(host[0])
            print(host[0])
            result.append(res)
        except Exception:
            result.append('err')

        values = []
        if res == 'err':
            values.append("err")
        else:
            if res.emails:
                if isinstance(res.emails, list):
                    a = ""
                    for r in res.emails:
                        a += r + '\n'

                    values.append(a)
                else:
                    values.append(res.emails)
            else:
                values.append("none")

        print(values)
        body = {
            "data": [
                {
                    "range": "with_google!Z" + str(i + 2),
                    "values": [values]
                }
            ],
            "valueInputOption": "RAW"
        }

        set_results = (
            sheet.values()
                .batchUpdate(spreadsheetId=SAMPLE_SPREADSHEET_ID, body=body)
                .execute()
        )
        i += 1

    # # print(result)
    # i=0
    # for res in result:
    #     values=[]
    #     if res=='err':
    #         values.append("err")
    #     else:
    #         if res.emails:
    #             if isinstance(res.emails, list):
    #                 a=""
    #                 for r in res.emails:
    #                     a+=r+'\n'
    #
    #                 values.append(a)
    #             else:
    #                 values.append(res.emails)
    #         else:
    #             values.append("none")
    #
    #     print(values)
    #     body = {
    #         "data": [
    #             {
    #                 "range": "with_google!Z" + str(i + 2),
    #                 "values": [values]
    #             }
    #         ],
    #         "valueInputOption": "RAW"
    #     }
    #
    #     set_results = (
    #         sheet.values()
    #             .batchUpdate(spreadsheetId=SAMPLE_SPREADSHEET_ID, body=body)
    #             .execute()
    #     )
    #     i+=1
