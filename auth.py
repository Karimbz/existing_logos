import logging
from pathlib import Path

# testing comits
import googleapiclient.discovery
from google.oauth2 import service_account

# If modifying these scopes, delete the file token.pickle.

SCOPES = ["https://www.googleapis.com/auth/spreadsheets",
          "https://www.googleapis.com/auth/drive.file","https://www.googleapis.com/auth/drive"]

SERVICE_ACCOUNT_FILE = Path(__file__).parent / "exisitng-logo-a5961426592c.json"
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def get_service_client(email_impersonated: str, name: str, v: str):
    credentials = service_account.Credentials.from_service_account_file(
        SERVICE_ACCOUNT_FILE, scopes=SCOPES
    )
    delegated_credentials = credentials.with_subject(email_impersonated)
    service_client = googleapiclient.discovery.build(
        name, v, credentials=delegated_credentials, cache_discovery=False
    )
    return service_client


def get_client(email_impersonated: str):
    credentials = service_account.Credentials.from_service_account_file(
        SERVICE_ACCOUNT_FILE, scopes=SCOPES
    )
    delegated_credentials = credentials.with_subject(email_impersonated)

    return delegated_credentials
