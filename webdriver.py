from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from auth import get_service_client



# The ID and range of a sample spreadsheet on Foubs
SAMPLE_SPREADSHEET_ID = "1oqKlwoBojQG1n2seNftOKNYKSPLS0sCAsrMMSjmQiVE"
SAMPLE_RANGE_NAME = "with_google!X2:X"
service=get_service_client('devmaster@sullivan-c.com','sheets','v4')

# Call the Sheets API
sheet = service.spreadsheets()
result = (
    sheet.values()
        .get(spreadsheetId=SAMPLE_SPREADSHEET_ID, range=SAMPLE_RANGE_NAME)
        .execute()
)
# print(sheet.)
values = result.get("values", [])

if not values:
    print("No data found.")
else:
    i=0
    for row in values:
        if i<1340:
            i+=1
            print(i)
            continue
        print(row[0])
        try:
            driver = webdriver.Chrome()

            driver.get("https://toolbox.googleapps.com/apps/checkmx/")
        except Exception:
            print('test')
        # # assert "Python" in driver.title
        elem = driver.find_element_by_id("mxdomain")
        elem.clear()
        elem.send_keys(row[0])
        elem.send_keys(Keys.RETURN)

        critical_all = driver.find_elements_by_css_selector(
            '.checkmx-result-row-err > .checkmx-test-description > .checkmx-test-name')
        warn_all = driver.find_elements_by_css_selector(
            '.checkmx-result-row-warn > .checkmx-test-description > .checkmx-test-name')
        infos_all = driver.find_elements_by_css_selector(
            '.checkmx-result-row-info > .checkmx-test-description > .checkmx-test-name')
        all=[]
        for a in critical_all:
            all.append("CRITICAL: %s" % a.text)
        for a in warn_all:
            all.append("WARN: %s" % a.text)
        for a in infos_all:
            all.append("INFO: %s" % a.text)
        body={
              "data": [
                {
                  "range": "with_google!Z"+str(i+2),
                  "values":[all]
                }
              ],
              "valueInputOption": "RAW"
            }
        print(all)

        set_results = (
            sheet.values()
                .batchUpdate(spreadsheetId=SAMPLE_SPREADSHEET_ID,body=body)
                .execute()
        )


        # print(elem.text)
        # assert "No results found." not in driver.page_source
        driver.close()
        i+=1
        # Print columns A and E, which correspond to indices 0 and 4.

