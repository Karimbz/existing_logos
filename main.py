from sc_py_ressources.df_playground.csv_files import read_csv
from sc_py_ressources.df_playground.csv_files import write_csv

from pathlib import Path
import csv
import dns.resolver
import logging
import tempfile

logger = logging.getLogger(__name__)


# ALL_ENTREPRISES = Path(__file__).parent / "sc_py_ressources/df_playground/data_samples/products.csv" \
ALL_ENTREPRISES = (
    Path(__file__).parent
    / "sc_py_ressources/df_playground/data_samples/EntreprisesFR100a5000.csv"
)

# read_csv(ALL_ENTREPRISES)

headers = []
google = []
not_google = []
out_with_error = []


with open(ALL_ENTREPRISES) as csv_file:
    read_csv = csv.reader(csv_file, delimiter=",")
    i = 0
    names = []
    urls = []
    new_list = []

    for row in read_csv:
        # print(row)
        if i == 0:
            headers = row
            i += 1
            continue
        j = 0
        row_with_headers = {}
        for n in row:
            row_with_headers.update({headers[j]: n})
            j += 1
        new_list.append(row_with_headers)
        i += 1
        # if i == 150 :
        #     break

    # for e in new_list:
    #     print(e.get('name'))

    i = 0
    for e in new_list:
        i += 1
        url = e.get("Url Canonical")
        name = e.get("denominationUniteLegale")
        # print(url,name)
        try:

            result = dns.resolver.query(url, "MX")
            for mxval in result:
                if mxval.to_text().__contains__("google"):
                    print("Conatins***** : ", name, " is : ", mxval.to_text())
                    google.append(e)

                    break

                print(
                    i,
                    "MX of : ",
                    name,
                    " is : ",
                    mxval.to_text(),
                    "contains google ?",
                    mxval.to_text().__contains__("google"),
                )
            if not mxval.to_text().__contains__("google"):
                not_google.append(e)
        except Exception:
            content = False
            out_with_error.append(e)
            logger.exception("Problem ")


with open("with_google", "w", newline="") as f:
    fieldnames = headers
    my_writer = csv.DictWriter(f, fieldnames=fieldnames)
    my_writer.writeheader()
    for e in google:
        my_writer.writerow(e)
with open("not_google", "w", newline="") as f:
    fieldnames = headers
    my_writer = csv.DictWriter(f, fieldnames=fieldnames)
    my_writer.writeheader()
    for e in not_google:
        my_writer.writerow(e)
with open("out_with_error", "w", newline="") as f:
    fieldnames = headers
    my_writer = csv.DictWriter(f, fieldnames=fieldnames)
    my_writer.writeheader()
    for e in out_with_error:
        my_writer.writerow(e)
    # write_csv(google, "test")
